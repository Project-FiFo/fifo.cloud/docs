+++
title = "Index"
date = 2017-11-29T23:46:32-08:00
draft = false
+++

# Fifo.Cloud Documentation
Classical cloud management solutions like OpenStack, SDC, or even Project-FiFo itself require you to colocate the management infrastructure alongside the hosts. Colocating the management infrastructure comes with some problems, starting from cost over enforced locality to the knowledge necessary to run cloud management solutions.

With FiFo Cloud we are taking a different approach to the problem. The fault tolerant architecture that lays the foundation of FiFo allows us to move the management infrastructure offside with minimal risk to running applications. This completely removes infrastructure cost, knowledge costs and provides management to span many sites and even different providers

Fifo.cloud retains Project-FiFo’s ability to scale to many hosts and manage massive numbers of containers, but it also eliminates the overhead of running on-premise Project-FiFo. With all the orchestration provided as a service, only a small agent must be installed on the host. FiFo-agent requires only a few dozen megabytes of memory, contrasted to other solutions that need gigabytes or even entire servers’ worth of resources.
