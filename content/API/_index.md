+++
title = "API"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 1000
+++

Fifo.Cloud's API is build in a RESTful manner, on which we put a high value. We aim to have a robust API that offers the best possible experience when implementing other components for it.
