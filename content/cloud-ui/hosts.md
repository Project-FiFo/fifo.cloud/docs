+++
title = "Hosts"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 200
+++

A host is a system, either physical or virtual, that has a fifo-agent installed. Zones, KVMs or jails that you create on your system do **not** count against the hosts.

As long as your system can establish an outbound HTTP(s) connections and a TLS connection to port `7777` on our API endpoints you can connect it to FiFo.cloud, no inbound connections are required.

## Overview
![overview](/images/cloud-ui/hosts-overview.png "Overview")

The host section is where you get an overview of the hosts you currently have connected.

## Adding
![new](/images/cloud-ui/hosts-new.png "New")

To add a new host click the big `+` sign in the overview. You will be able to enter a region and a name for the host. The name is just for identification. In addition to the name the region allows grouping hosts and in larger plans is used for [stacks and clus

## Details
![show](/images/cloud-ui/hosts-show.png "Show")

Selecting a host will show it's detailed. The page provides some information:

* The region
* The operating system running on the host
* The architecture
* The networks available on the host
* When the host was last seen (this is only periodically updated every few minutes)
* The protocol version used by the fifo-agent installed on the host
* How much memory zones, jails or KVM use
* How much space is left on the ZFS pool

Larger packages also allow adding, changing and removing [characteristics](/cloud-ui/characteristics) for hosts. You can read more about this feature in our [characteristics howot](/howtos/characteristics).

For VPN enabled accounts the VPN can be enabled for a host here and the VPN IP is shown here.

If you run servers in your own datacenter it is also possible to use the identify button (the small light) to trigger a ipmitool identify.


## Deleting

Hosts can only be deleted after they've been disconnected for 5 minutes. At that point a little trash can icon will be shown on the top right.
