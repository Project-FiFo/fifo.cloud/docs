+++
title = "SSH Keys"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 600
+++

FiFo.cloud manages SSH keys for you. These keys have two effects. When creating a new jail or KVM the keys will be written to the `/root/.ssh/.authorized_keys` file (given the image provides that functionality). For zones fifo.cloud allows for dynamic key management based on the currently set keys. 

## Overview

![overview](/images/cloud-ui/keys-overview.png "Overview")

In the overview you can see all keys currently assigned to your account. You can delete existing keys or add new ones here. Changes here will have direct effect on zones, however for KVM or jails only the keys at creation time are taken into account.


## Adding

![new](/images/cloud-ui/keys-new.png "New")

Adding a new key requires you to enter a name for the key - to help identify it, and your **public** SSH key (ending with `.pub`)