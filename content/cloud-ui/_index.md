+++
title = "Cloud UI"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 100
+++

The FiFo.cloud UI is the user interface for FiFo.cloud, it provides access to all the relevant functionality along with account management.

## Overview

![overview](/images/cloud-ui/overview.png "Overview")


## Navigation

The main nav bar on the left allows you to access all sections of FiFo.cloud from top to bottom the sections are:

* Overview
* [Hosts](/cloud-ui/hosts)
* [VMs](/cloud-ui/vms)
* [Packages](/cloud-ui/packages)
* [Networks](/cloud-ui/networks)
* [Keys](/cloud-ui/keys)
* [Account](/cloud-ui/account)
* Logout
