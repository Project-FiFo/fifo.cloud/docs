+++
title = "Packages"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 400
+++

Packages are the abstraction over machine size in fifo.cloud, they specify all the resources that a machine receives.

## Overview
![overview](/images/cloud-ui/packages-overview.png "Overview")

In the overview, you get to see a list of all packages currently created by you along with the option to create new packages.


## Adding
![new](/images/cloud-ui/packages-new.png "New")

When adding a new package, there are a few variables to define:

* **Name** - the name, it needs to be unique
* **CPU** - the percentage of CPU cores for the package, `100%` means one core
* **Memory** - the memory for the machine in megabyte
* **Quota** - the disk quota for the machine


## Details
![show](/images/cloud-ui/packages-show.png "Show")

In the detail view, you see all the settings you made for a package. It is not possible to change resources after creation, but you can add and remove [characteristics](/cloud-ui/characteristics). 

