+++
title = "VMs"
draft = false
weight = 300
+++

The VMs section shows a list of VMs that are on your hosts. Please not that we use the term VM liberally here to cover both, KVMs, Solaris Zones and FreeBSD Jails.

## Overview
![overview](/images/cloud-ui/vms-overview.png "Overview")

The VMs section shows a list of VMs that are on your hosts. The overview allows you to get a high level view of your current deployment. Clicking on the `+` to create a new VM.

## Creating
![new](/images/cloud-ui/vms-new.png "New")

Creating a new VM will take you through a wizard that'll let you you define a the attributes. 

* The **alias** is just an identifying name for the UI that helps to keep different VMs apart.
* The **hostname** needs to conform to hostname rules
* The **dataset** defines what base dataset to use, this affects the installed operating system (for KVMs) or the ZFS filesystem (for zones and jails). As a side effect, this also affects what type of host the VM will be deployed on, KVMs and Zone get deployed on SmartOS and Jails on FreeBSD.
* The **package** defines the resources the VM has at its disposal.
* The **network** defines the network used on the interface of the VM and its IP, gateway, netmask and initial resolvers.
* **Delegated dataset** defines if a delegated dataset should be used for the VM, this will however make it impossible to use snapshots and backups on this VM. (SmartOS Only)

## Details
![show](/images/cloud-ui/vms-show.png "Show")

When selecting one of the VMs from the overview, you can see it's detailed. Here you see the essential attributes of the VM, its package and the networking assigned to it. You can also take a look at the VM creation logs in case of problems to help you debug your host or provide information on tickets.

At the top right you have the option to start, stop, restart and delete your VM. You can edit it to change package or alias or open a console to connect to the VNC or Terminal console for KVMs or Zone and Jails respectively.

FiFo.cloud DNS will automatically create DNS entries for each of your machines on the primary interface. In addition to that you can add dedicated hostnames to each interface to have then resolved automatically. Should two or more VMs share a hostname within your account it will return all related IPs on a DNS request. 

If the VM is in stopped state it is possible to add and remove network cards.
