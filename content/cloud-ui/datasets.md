+++
title = "Datasets"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 600
+++

On the datasets page you can see the datasets currently available to you. These are either the community datasets picked by fifo.cloud or your own if you have set up your custom s3 backend.


![overview](/images/cloud-ui/datasets-overview.png "Overview")


## Import

If you have set a custom s3 config you can import your own dataserts. You either pick from one of the dataset mirrors we already linked and use existing datasets or provide your own dataset via an URL. If you provide your own URL it has to be on a imgapi compatible endpoint.

Datasets imported this way will be stored on the s3 endpoint provided by you.

![overview](/images/cloud-ui/datasets-import.png "Import")
