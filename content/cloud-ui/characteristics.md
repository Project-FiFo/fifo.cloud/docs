+++
title = "Characteristics"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 800
+++

Characteristics are used to allow for advanced provisioning rules for FiFo.cloud. They are only available in the Medium and Large package.

Packages that have a given characteristic will only allow machines with this package to deployed on hosts with he same characteristic.

So for example, if you set a characteristic `disk` to the value `ssd` on one host and `disk` to `hdd` on another you can now create packages that either only deploys on hosts with either SSD or HDDs.
