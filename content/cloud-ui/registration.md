+++
title = "Registration"
draft = false
weight = 100
+++

To register enter your Company or organisation, along with an e-mail address that will function as your login and the password you wish to use.

You will then receive an activation email with a link to follow, once clicking that your account is activated and you can start using FiFo.cloud - you can look at our [getting stared](/howtos/getting-started) guide to get an idea where to go next.

![signup](/images/cloud-ui/signup.png "Signup")


Please note that the free tier is only available on the [try instance](https://try.fifo.cloud) and it is not possible to up or downgrade form there.
