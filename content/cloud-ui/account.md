+++
title = "Account"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 700
+++

The account page shows information about your account. It shows the selected plan along with the option to change it. An option to change your password is provided here as well.


![overview](/images/cloud-ui/account-overview.png "Overview")

## Usage

The usage screen shows the number of concurrent hosts used in the current month on a daily basis.

![overview](/images/cloud-ui/account-usage.png "Usage")

## Invoices

In the invoice section you can see your past invoices and their status.

![overview](/images/cloud-ui/account-invoices.png "Invoices")

## API Tokens

The API key section shows a list of existing API keys along with the ability to generate new ones. API keys are static keys that do not become invalid over time. Please note that when creating a new API key it is shown **precisely one time** and cannot be viewed again.

![overview](/images/cloud-ui/account-tokens.png "API Tokens")

## Users

In the users section you can invite team mates to your FiFo.cloud account.

![overview](/images/cloud-ui/account-users.png "Users")

## S3 Config

For the Medium and Large plan it is possible to use a constum S3 configuration. A custom S3 endpoint allows you to impot and manage your own [datasets](/cloud-ui/datasets) instead of using the community datasets that are offered as part of FiFo.cloud.

![overview](/images/cloud-ui/account-s3.png "S3 Config")
