+++
title = "5. Create host on Packet"
date = 2017-11-29T23:46:32-08:00
weight = 150
+++

Using the Packet.net WebUI navigate to the project you would like your host to be in. On the "Servers" tab select "Deploy Server"

![my image](/images/public-cloud-ip/packet-create-host.png)  

**Hostname** Anything

**Type** Type 1 or better. 

**OS** FreeBSD 11.0

**Location** Same location as your requested ip range.

**Options** 
In the creation metadata use the script output by fifo hypervisor page, append the following at the end of the script but before 'reboot'.  

**MAKE SURE to substitute 'Packet{Packet DC Airport Code}' (line2) for the nic tag that you used when creating the Fifo.Cloud network.**

{{< highlight shell >}}
pkg install -y jq
/usr/bin/sed -i "" 's/admin/Packet{Packet DC Airport Code}/g' /usr/local/etc/vmadm.toml
echo 'gateway_enable="YES"' >> /etc/rc.conf
echo 'ifconfig_bridge0_alias0="inet 192.168.255.255 netmask 255.255.255.255"' >> /etc/rc.conf
echo 'fifo_cloud_routes_enable="YES"' >> /etc/rc.conf
mkdir -p /usr/local/etc/fifo.cloud/
touch /usr/local/etc/fifo.cloud/global.routes
curl -o /usr/local/etc/rc.d/fifo-cloud-routes https://gitlab.com/Project-FiFo/fifo.cloud/examples/raw/master/networking/PublicIP/Packet.net/fifo-cloud-routes
chmod +x /usr/local/etc/rc.d/fifo-cloud-routes
reboot
{{< /highlight >}}



Using the big blue botton deploy your device. Once device is created use emergency console to watch for when metadata script is done.

Lastly you will need to ssh to your newly created Packet device and add your Packet API key created in step 2.

{{< highlight shell >}}
echo "71EAWklvUeN9VBY4zjALDMQrB6Jr1fD7" > /usr/local/etc/fifo.cloud/packet.key
{{< /highlight >}}