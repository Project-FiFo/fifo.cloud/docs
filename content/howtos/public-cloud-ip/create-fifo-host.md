+++
title = "4. Create host on FiFo.Cloud"
date = 2017-11-29T23:46:32-08:00
weight = 140
+++

In the Fifo.Cloud user interface click the "Hosts" tab ( {{<icon tv>}} ). Then select New ({{<icon stack circle plus >}}).  You will be able to enter a region and a name for the host. The name is just for identification. In addition to the name the region allows grouping hosts and in larger plans is used for stacks and clusters.

Once the host is created select the "Packet (FreeBSD)" tab to see the script for installing the fifo-agent. Copy this script to your clipboard and proceed to "Create host on Packet".


{{% notice tip %}}
The install script will by default create a zpool with only the first disk. If you would like to use all disk remove `head -n 1` from the line starting with `sort /var/tmp/disk.all /var/tmp/disk.used ...`
{{% /notice %}}