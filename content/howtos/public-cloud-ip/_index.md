+++
title = "Packet.Net Public IP Addresses"
date = 2017-11-29T23:46:32-08:00
weight = 200
+++

*Estimated time 35 minutes.*

Like many other cloud providors Packet.net provides the ability to assign additional IP addresses via Elastic IPs. When Packet Elastic IPs are assigned to a host Packet directs all traffic for that IP to the network interface on that host. The trick to using this setup is that all traffic outgoing must be routed through the management IP address. 

The following steps will be necessary to request IP address from Packet, make them assignable in FiFo.cloud, and provision a jail with the public IPs:

{{% children style="div" depth="1" %}}

With the steps shown in this tutorial you will be able to create a range of IPs that are shared among multiple Packet servers. To add additional servers that will share the same range of IPs repeat steps 4 & 5 as necessary.
