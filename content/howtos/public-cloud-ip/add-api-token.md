+++
title = "2. Create Packet API Token"
date = 2017-11-29T23:46:32-08:00
weight = 120
+++

In the Packet.net portal navigate to the "API Keys" (https://app.packet.net/portal/#/api-keys).
Genereate a new API key with any description you would like ("FifoNetworkScript" is what we used), and Read/Write permissions.