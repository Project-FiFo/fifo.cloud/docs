+++
title = "1. Request Packet IP Range"
date = 2017-11-29T23:46:32-08:00
weight = 110
+++

An elastic IP range must be requested from Packet in order to allow multiple container hosts the ability to have guests from the same IP pool.

Start on the "Manage" menu item and then click the project to which you would like to add the pool. Using the "IP & Networks" tab select "Request More IP Addresses".

![my image](/images/public-cloud-ip/packet-request-ip.png#right?width=350px)  

<br/><br/>

**Address Type**: Public IPv4

**Location**: Sunnyvale, CA (or whatever datacenter your hosts will be in)

**Quantity**: Any size will work. This tutorial uses /27. From our experince packet will automatically approve requests for /28 or smaller without any delay.

**Comments**: Explain your use. In the past we have used comments as simple as "dynamically created containers" with success.

<div style="clear: both;"></div>