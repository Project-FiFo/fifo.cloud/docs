+++
title = "3. Add Fifo.Cloud IP Range"
date = 2017-11-29T23:46:32-08:00
weight = 130
+++

Once you have recieved an IP range assignment from Packet you must add it to Fifo.Cloud. Due to how FiFo assigns IP address we need to configure the network in such a way that any host can listen for the traffic and pass it to the container. This involves having a gateway IP that is assinged to the bridge interface to route traffic out on. There is some more explination about how this all works at this blog post. 

In the Fifo.Cloud user interface click the Networks tab. Then select New. 

![my image](/images/public-cloud-ip/fifo-network-create.png#right?width=350px)  

**Name** your network whatever you want but it makes sense to somehow note what datacenter the IPs can be used in. 

**NIC Tag** anything is ok, but it makese sense to keep things consistnat. Also NOTE this needs to match your vmadm config (this is set in "Create Fifo Host" step).

**VLAN** should be left blank (we are not tagging)

**Subnet** should be the first IP of the assigned range

**Netmask** should be 255.255.255.255 regardless of the actual assignment size. This is because we always want all traffic to route out becuase there is no guarentee that every container with an IP in the range will end up on the same host.

**Gateway** 192.168.255.255 This is the IP we will assing the bridge on our host.

**First** will the the first IP in the range assigned (same as Subnet value above)

**Last** will be the last IP in the range assigned.

**NS1/NS2** servers can be anything of your choosing.

Lastly you will find two fields for "Create" and "Delete" scripts. These scripts are run at the creation and deletion of any container that uses this network. We will use these scripts to setup routing and to attach the IP to the host at creation time and at deletion time, remove the route and free the IP. We also add the route add command to a script that can be enabled to run incase the host is rebooted (routes are not persistant). 

**Create Script**:
{{< highlight shell >}}
#!/bin/sh
export PATH=$PATH:/usr/local/bin/:/usr/local/sbin/
VM_UUID=$1
VM_IP=$2
PACKET_TOKEN=`cat /usr/local/etc/fifo.cloud/packet.key`

NIC_TAG=`vmadm get $VM_UUID | jq -r '.nics | .[] | select(.ip=="'$VM_IP'") | .nic_tag'`
GZ_IF_NAME=`awk -F "=" '/'$NIC_TAG'/ {print $2}' /usr/local/etc/vmadm.toml | tr -d ' ' | tr -d '"'`
JAIL_IF_NAME=`vmadm get $VM_UUID | jq -r '.nics | .[] | select(.ip=="'$VM_IP'") | .interface'`

# Associate IP with hypervisor
echo '{"address":"'$VM_IP'/32","manageable":"false"}' | curl -H 'X-Auth-Token: '$PACKET_TOKEN -d @- -H "Content-Type: application/json" https://api.packet.net/devices/`curl -s https://metadata.packet.net/metadata | jq -r .id`/ips

# append string to file if missing
# usage: appendifmissing <file> <string>
appendifmissing ()
{
    grep -q -F "${2}" "${1}" || echo "${2}" >> "${1}"
}

# Setup Routing to VM
appendifmissing /usr/local/etc/fifo.cloud/global.routes "route add ${VM_IP} -iface $GZ_IF_NAME"
route add ${VM_IP} -iface $GZ_IF_NAME

#Setup Routing from VM
echo '{"set_routes": {"192.168.255.255": "'$JAIL_IF_NAME'"}}' | vmadm update $VM_UUID

{{< /highlight >}}

**Delete Script**:
{{< highlight shell >}}
#!/bin/sh
set -x
export PATH=$PATH:/usr/local/bin/:/usr/local/sbin/
VM_UUID=$1
VM_IP=$2
PACKET_TOKEN=`cat /usr/local/etc/fifo.cloud/packet.key`
ESCAPED_IP=`echo $VM_IP | sed 's/\./\\\\./g'`

sed -i "" '/'$ESCAPED_IP'/d' /usr/local/etc/fifo.cloud/global.routes
route delete $VM_IP
{{< /highlight >}}
