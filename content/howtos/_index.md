+++
title = "Howtos"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 200
+++

In the Howto section, we try to provide some howtos for everyday tasks that help you get started quickly. Guest submissions are more than welcome; you can open a Ticket and PR [here](https://gitlab.com/Project-FiFo/fifo.cloud/docs).
