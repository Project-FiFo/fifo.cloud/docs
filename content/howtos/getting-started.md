+++
title = "Getting Started"
date = 2017-11-29T23:46:32-08:00
draft = false
weight = 100
+++

This little tutorial will help you get started with FiFo.cloud. To get up and running only a few steps are required. Since you arrived here, it seems like you already went through the first one, which is signing up.

To create a first VM, you will need to take the following steps:

1. a host
2. a package
3. a network

## The host

We'll start with the host. To add your first host navigate to the [hosts section](/cloud-UI/hosts) and click the big round `+` button. For now, leave the region empty as it will not affect this tutorial and pick a good name for your first host. Then click the add button.

The UI will forward you to the install screen for the host. Here we offer you some options, select the one that matches your host. SmartOS when you are running SmartOS, or FreeBSD when you are running FreeBSD. In addition to the two we offer some quick setup scripts if you want to run FiFo.cloud on either [Packet](https://packet.net) or [Digital Ocean](https://digitalocean.com) using their respective metadata feature.

To install fifo-agent all, you need to do is to copy the command given. The installer will ask a few questions and then install the agent. The agent will automatically start after a few moments, and register then will turn active in the UI. In the meantime, we can continue with the next steps.

## The package

Packages define the size and resources a VM has. We'll create a simple one here and not go into the details about characteristics. To get this going navigate to the [packages section](/cloud-UI/packages) and press the big round `+` button.

You will have to specify for a name and the resources the package will use. We select `100%` CPU, `1024 MB` of memory and `5 GB` of disk space. While memory and disk are probably self-explanatory, it's worth mentioning that the % of CPU stands for % of a core. So 100% CPU means 1 CPU core, not 100% of all CPU cores.

Press the create button to finish the package creation.

## The network

Last but not least we're going to create a network. The network creation is the most complex of the tasks. You will need a network range you can reach. There is [a tutorial how to do this on package](/howtos/packet-public-IP) with public IP addresses, but every range that you can reach will work.

The nic-tag is most likely going to be `admin` - the network available on your host and VLAN ID can be left blank for now. The subnet is going to be the subnet IP from your network (for example `192.168.0.0`). The netmask will be your netmask (for example `255.255.255.0`). The gateway is your network's gateway (for example `192.168.0.1`).

The first and last address does not mean the first and last address in the whole range but rather the first and last one to be handed out, it can be good to spare some IPs and only pick a subsection of your available range (for example `192.168.0.100` to  `192.168.0.200`).

You can optionally add nameservers otherwise `8.8.8.8` and `8.8.4.4` will be selected. The script section will be left empty for now. You can find an example of how to use them in the tutorial mentioned above.


## Creating the first VM

At this point, it's worth checking if the host successfully registered, if so we can continue. Navigate to the [vms section](/cloud-UI/VMS). Give your VM a name, and select the network and package we just created. Make sure to pick a dataset that fits your host, so a FreeBSD jail if you're running FreeBSD and either zone or KVM when you're running SmartOS.

Click the create button and start off the creation. Since the dataset will need to be imported to the host this step can depending on your connection, take a few minutes.

Once the host finished creating the VM, you can use the console (the leftmost button on the top) to connect to the console of your zone.
